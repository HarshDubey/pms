import React, { Component } from 'react'
import Login from './screen/page/Login';
import SignUp from './screen/page/SignUp';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
export class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Login} />
          <Route exact path='/signup' component={SignUp} />
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App
