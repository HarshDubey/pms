import React, { Component } from 'react'
import {
    Avatar, Button, CssBaseline, TextField, FormControlLabel, Checkbox,
    Link, Grid, Box, Typography, Container
} from '@material-ui/core';
import { NavLink } from 'react-router-dom';
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: "signup"
        }
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        return (
            <Container component="main" maxWidth="xs" style={{ marginTop: 10 }}>
                <h2>PASSWORD MANAGEMENT SYSTEM</h2>
                <CssBaseline />
                <div>
                    <Typography component="h1" variant="h3">
                        Sign Up
                    </Typography>
                    <form noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={this.handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={this.handleChange}
                        />
                        <FormControlLabel
                            control={<Checkbox value="agree" color="primary" onChange={this.handleChange} />}
                            label="Agree With Terms & Conditions"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={{ marginTop: 10 }}
                        >
                            Sign Up
                        </Button>
                        <Grid container style={{ marginTop: 10 }}>
                            <Grid item>
                                <NavLink exact to='/' variant="body2">
                                    {"Already have an account? Sign In"}
                                </NavLink>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        )
    }
}

export default SignUp
