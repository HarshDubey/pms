import React, { Component } from 'react';
import {
    Avatar, Button, TextField, FormControlLabel, Checkbox,
    Link, Grid, Box, Typography, Container, Card, CardHeader
} from '@material-ui/core';
import { NavLink } from 'react-router-dom';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: "login"
        }
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        return (
            <Container component="main" maxWidth="sm" style={{ marginTop: 10 }}>
                <Card style={{ padding: 10 }}>
                    <div>
                        <Typography component="h1" variant="h5">
                            PASSWORD MANAGEMENT SYSTEM
                        </Typography>
                        <Typography component="h1" variant="h4">
                            Sign in
                        </Typography>
                        <form noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                onChange={this.handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={this.handleChange}
                            />
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" onChange={this.handleChange} />}
                                label="Remember me"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                style={{ marginTop: 10 }}
                            >
                                Sign In
                        </Button>
                            <Grid container style={{ marginTop: 10 }}>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Forgot password?
                                </Link>
                                </Grid>
                                <Grid item>
                                    <NavLink exact to="/signup" variant="body2">
                                        {"Don't have an account? Sign Up"}
                                    </NavLink>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Card>
            </Container>
        )
    }
}

export default Login;
